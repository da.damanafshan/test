import React, { useContext } from "react";
import { BrowserRouter, Route, Navigate, Routes } from 'react-router-dom';
import Login from "./pages/login/Login";
import Home from "./pages/home/Home";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "./App.css";
import AuthContext from "./context/AuthProvider";


const App = () => {

  const { auth } = useContext(AuthContext);

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path={"/login"} element={<Login />} />
          <Route
            path="/"
            element={
              <PrivateRoute auth={auth.accessToken}>
                <Home />
              </PrivateRoute>
            }
          />
        </Routes>
      </BrowserRouter>
      <ToastContainer />
    </div>
  )
}

const PrivateRoute = ({ auth, children }) => {
  return auth ? children : <Navigate to="/login" />;
};

export default App;
