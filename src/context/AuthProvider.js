import { createContext, useState } from "react";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
    const [auth, setAuth] = useState({});
    const [categorieslist, setCategorieslist] = useState([]);

    return (
        <AuthContext.Provider value={{ auth, setAuth, categorieslist, setCategorieslist }}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext;