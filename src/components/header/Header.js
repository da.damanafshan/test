import classNames from 'classnames';
import React, { useContext, useRef, useState } from 'react';
import { toast } from 'react-toastify';
import AuthContext from '../../context/AuthProvider';
import Button from '../button/Button';
import styles from "./Header.module.css"

const Header = () => {
    const categorieValue = useRef()
    const { categorieslist, setCategorieslist } = useContext(AuthContext);
    const [showModal, setShowModal] = useState(false);
    const [categoryName, setCategoryName] = useState({});

    const handleCategory = () => {
        if (categoryName.name !== "") {
            const isCategory = categorieslist.filter(i => i.name === categoryName.name)
            if (isCategory.length < 1) {
                setCategorieslist([...categorieslist, categoryName]);
                setCategoryName({});
                setShowModal(false);
                categorieValue.current.value = ""
            } else {
                toast.warn("دسته بندی با این نام وجود دارد")
            }
        }
    }

    return (
        <>
            <div className={classNames(styles.modal, showModal === true ? `${styles.show}` : `${styles.hide}`)}>
                <div className={styles.main}>
                    <h3>
                        New Category Modal
                    </h3>
                    <label htmlFor='newCategory'>
                        Title
                    </label>
                    <input type="text" name="newCategory" ref={categorieValue} onChange={e => setCategoryName({ name: e.target.value })} />
                    <div className={styles.buttons}>
                        <Button text="Add" onClick={handleCategory} />
                        <Button text="Close" onClick={e => setShowModal(!showModal)} />
                    </div>
                </div>
            </div>
            <div className={styles.container}>
                <h4>Category</h4>
                <Button text="New Category" onClick={e => setShowModal(!showModal)} />
            </div>
        </>
    )
}

export default Header