import { useRef, useState, useEffect, useContext } from 'react';
import AuthContext from "../../context/AuthProvider";
import { useNavigate } from "react-router-dom";
import axios from '../../api/axios';
import { toast } from 'react-toastify';
import "./Login.module.css";
import Button from '../../components/button/Button';

//URL
const LOGIN_URL = '/auth/login';

const Login = () => {

  const navigate = useNavigate();
  const { setAuth } = useContext(AuthContext);
  const userRef = useRef();

  //State
  const [user, setUser] = useState('');
  const [pwd, setPwd] = useState('');

  useEffect(() => {
    userRef.current.focus();
  }, [])

  //Validate
  const validateLogin = (user) => {
    if (!user.username) {
      return "یوزرنیم را وارد کنید"
    } else if (user.username.length < 5) {
      return "یوزرنیم را به صورت صحیح وارد کنید"
    } else if (!user.password) {
      return "پسورد را وارد کنید"
    } else if (user.password.length < 5) {
      return "پسورد باید بیشتر از 4 عدد باشد"
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const loginData = {
      username: user,
      password: pwd
    }

    const error = validateLogin(loginData);
    if (error)
      return toast.error(error);
    try {
      const response = await axios.post(LOGIN_URL,
        JSON.stringify(loginData),
        {
          headers: { 'Content-Type': 'application/json' },
        }
      );
      const accessToken = response?.data?.payload.token;
      const id = response?.data?.payload.user.id;
      const firstName = response?.data?.payload.user.firstName;
      const lastName = response?.data?.payload.user.lastName;
      const avatar = response?.data?.payload.user.avatar;
      setAuth({ user, pwd, accessToken });
      setUser('');
      setPwd('');
      localStorage.setItem("username", user)
      localStorage.setItem("id", id)
      localStorage.setItem("firstName", firstName)
      localStorage.setItem("lastName", lastName)
      localStorage.setItem("avatar", avatar)
      toast.success("شما با موفقیت وارد شدید")
      navigate("/", { replace: true });
    } catch (err) {
      if (!err?.response) {
        toast.error('server: No Server Response');
      } else {
        toast.error("یوزرنیم یا پسورد صحیح نمیباشد")
      }
    }
  }

  return (
    <>
      <section>
        <h1>Sign In</h1>
        <form onSubmit={handleSubmit}>
          <label htmlFor="username">Username:</label>
          <input
            type="text"
            id="username"
            ref={userRef}
            autoComplete="on"
            onChange={(e) => setUser(e.target.value)}
            value={user}

          />

          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            onChange={(e) => setPwd(e.target.value)}
            value={pwd}

          />
          <Button text="Submit" />
        </form>
      </section>
    </>
  )
}

export default Login