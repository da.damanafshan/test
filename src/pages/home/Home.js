import classNames from 'classnames';
import React, { useContext, useState } from 'react'
import axios from '../../api/axios';
import Button from '../../components/button/Button';
import Header from '../../components/header/Header'
import AuthContext from '../../context/AuthProvider';
import styles from "./Home.module.css"

//URL
const BOOKS_URL = '/books';

const Home = () => {

  //Context
  const { categorieslist, setCategorieslist } = useContext(AuthContext);

  //States
  const [booksList, setBooksList] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [checkedItem, setCheckedItem] = useState([]);

  const handlerBooks = async (item) => {
    const response = await axios.get(BOOKS_URL);
    const getBooks = response.data.payload;
    const newListBook = getBooks.map((element) => ({
      ...element,
      nameCategory: item
    }));
    setBooksList(newListBook);
    setShowModal(true);
  }

  const handlerDelete = (id, name) => {
    let newListBook = categorieslist.map((i) => {
      const items = [...i.bookList];
      const filteredItem = items.filter((item) => item.id !== id);
      return i.name === name ?
        i = {
          ...i,
          bookList: filteredItem
        }
        : i
    });
    setCategorieslist(newListBook);
  }

  const handlerSelectCheckedItem = (e, item) => {
    const checked = checkedItem.map(i => i.id)
    if (checked.includes(item.id)) {
      return;
    } else {
      const checkedList = [...checkedItem, item].map((element) => ({
        ...element,
        nameCategory: item.nameCategory
      }))
      return e ? setCheckedItem(checkedList) : null;
    };
  };


  const handlerNewCategoriesList = () => {
    let AddBooks = categorieslist.map((i) => {
      return i.name === checkedItem[0].nameCategory ?
        i = {
          ...i,
          bookList: i.bookList ? [...i.bookList, ...checkedItem] : checkedItem
        }
        :
        i
    });
    setCheckedItem([])
    setCategorieslist(AddBooks);
    setShowModal(false);
  };


  return (
    <>
      {/*.... Modal ....*/}
      <div className={classNames(styles.modal, showModal === true ? `${styles.show}` : `${styles.hide}`)}>
        <h2>
          Add Books To History
        </h2>
        <div className={styles.booksList}>
          {
            booksList.map((items, index) => {
              return (
                <>
                  <div className={styles.booksSelect} key={index}>
                    <input type="checkbox" checked={showModal === false ? false : null} onChange={(e) => handlerSelectCheckedItem(e.target.checked, items)} />
                    <p className={styles.title}>{items.title}</p>
                  </div>
                </>
              )
            })
          }
        </div>
        <div className={styles.buttons}>
          <Button text="Add Selected Books" onClick={handlerNewCategoriesList} />
          <Button text="Close" onClick={e => setShowModal(!showModal)} />
        </div>
      </div>

      {/*.... Main ....*/}
      <div className={styles.container}>
        <Header />
        <hr />
        <div className={styles.main}>
          {
            categorieslist.map((items, index) => {
              return (
                <>
                  <div className={styles.category} key={index}>
                    <div className={styles.header}>
                      <h4>{items.name}</h4>
                      <button className={styles.bookBtn} onClick={() => handlerBooks(items.name)}>Add Books</button>
                    </div>
                    <hr />
                    <div className={styles.allBooks}>
                      {items.bookList && items.bookList.map((item, index) => {
                        return (
                          <>
                            <div className={styles.book} key={index}>
                              <button className={styles.deleteBtn} onClick={() => handlerDelete(item.id, items.name)}>Delete</button>
                              <img src={item.thumbnail} alt={item.title} />
                              <p>{item.title}</p>
                            </div>
                          </>
                        )
                      })
                      }
                    </div>
                  </div>
                </>
              )
            })
          }
        </div>
      </div>
    </>
  )
}

export default Home;